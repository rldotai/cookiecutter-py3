# {{ cookiecutter.project_name }} 

{{ cookiecutter.description }}


## Features

- Lots of stuff, probably.

## Install for development

```bash
git clone https://gitlab.com/{{ cookiecutter.git_username }}/{{ cookiecutter.repo_name }}.git
cd {{ cookiecutter.repo_name }}
pip install --editable .
```


## License

MIT. See the [LICENSE](LICENSE) file for more details.