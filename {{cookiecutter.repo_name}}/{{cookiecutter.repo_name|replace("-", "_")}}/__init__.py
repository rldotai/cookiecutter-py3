"""
{{ cookiecutter.project_name }} - {{ cookiecutter.description }}
"""

__version__ = '{{ cookiecutter.version }}'
__author__ = '{{ cookiecutter.author }} <{{ cookiecutter.email }}>'

# don't export modules unless they're in the whitelist
import inspect
_whitelist = []

# define `__all__` for this package
__all__ = [name for name, x in locals().items() if not name.startswith('_') and
           (not inspect.ismodule(x) or x in _whitelist)]
